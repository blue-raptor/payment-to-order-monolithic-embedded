# payment-to-order use case

Implemented a docker instance of RabbitMQ

> %> docker run -d --hostname rabq --name bugsy -p 8088:15672 -p 5672:5672 rabbitmq:3-management

This code sample will send and receive a message to a designated remote queue running on server: 'thor'.

The sender and receiver processes are executing on a client Macbook which has a separate IPaddr.
