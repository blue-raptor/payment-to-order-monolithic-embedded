package com.blueraptor.p2o.controller;

import com.blueraptor.p2o.service.ReceiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


// Receive service call

@RestController
public class ConsumeController {
    ReceiveService receiverService;

    @Autowired
    public ConsumeController(ReceiveService receiverService) {
        this.receiverService = receiverService;
    }

    @RequestMapping(value = "/receive", method = RequestMethod.POST)
    public void receiveMessage() throws Exception {
        this.receiverService.consume();
    }
}
