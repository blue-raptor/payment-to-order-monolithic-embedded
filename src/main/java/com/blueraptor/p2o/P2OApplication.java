package com.blueraptor.p2o;

import com.blueraptor.p2o.service.ReceiveService;
import com.blueraptor.p2o.service.SendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class P2OApplication {
    @Autowired
    private SendService sendService;
    @Autowired
    private ReceiveService receiveService;

    public static void main(String[] args) throws IOException, TimeoutException {
        SpringApplication.run(P2OApplication.class, args);
    }

}
