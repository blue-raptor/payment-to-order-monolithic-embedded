package com.blueraptor.p2o.service;

import com.blueraptor.p2o.domain.Message;
import com.blueraptor.p2o.repo.MessageRepository;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.List;

@Service("receiverService")
public class ReceiveService {

    private MessageRepository messageRepository;
    private final static String QUEUE_NAME = "demo-q";
    static Logger logger = LoggerFactory.getLogger(ReceiveService.class);

    @Autowired
    public ReceiveService(MessageRepository messageRepository){
        this.messageRepository = messageRepository;
    }

    public void consume() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setHost("thor");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
            logger.info("[!] ReceiveService: Received '" + message + "'");
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });

        // The Consumer connects to the same Message Repository as the Publisher updated records.
        List<Message> allMessages = (List<Message>) this.messageRepository.findAll();
    }
}