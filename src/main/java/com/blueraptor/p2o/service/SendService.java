package com.blueraptor.p2o.service;

import com.blueraptor.p2o.domain.Message;
import com.blueraptor.p2o.repo.MessageRepository;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeoutException;

@Service("senderService")
public class SendService {
    static Logger logger = LoggerFactory.getLogger(SendService.class);

    private static final String QUEUE_NAME = "demo-q";

    private MessageRepository messageRepository;

    @Autowired
    public SendService(MessageRepository messageRepository){
        this.messageRepository = messageRepository;
    }

    /**
     *
     * @param message
     * @param source
     * @param targetQ
     * @return
     */
    public Message createMessage(String message, String source, String targetQ) {
        return messageRepository.save(new Message(message, source, targetQ));
    }

    public void fire(Message message) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUsername("guest");
        factory.setPassword("guest");
        factory.setHost("thor");
        factory.setPort(5672);
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME,false,false,false,null);
        String msgText = message.getMessage() + ": " + message.getId();
        channel.basicPublish("", message.getTargetQ(), null,msgText.getBytes(StandardCharsets.UTF_8));
        logger.info("[!] SendService: Sent '" + msgText + "'");
        channel.close();
        connection.close();
    }

}
